// ============ MAIN NAV ============

let mainMenu = document.getElementById('mainMenu');

let hamburgerButton = document.getElementById('hamburger');

hamburgerButton.addEventListener('click', ()=>{
    mainMenu.classList.toggle('navVisible');
})


// ============ CAROUSSEL PROJETS ============

let right1 = document.getElementById('right1');
let right2 = document.getElementById('right2');

let left2 = document.getElementById('left2');
let left3 = document.getElementById('left3');

let container1 = document.getElementById('container1');
let container2 = document.getElementById('container2');
let container3 = document.getElementById('container3');


right1.addEventListener('click', ()=>{
    container2.classList.add('container2-right');
    container1.classList.add('container1-right');
});

left2.addEventListener('click', ()=>{
    container2.classList.remove('container2-right');
    container1.classList.remove('container1-right');

});

right2.addEventListener('click', ()=>{
    container3.classList.add('container3-right');
    container2.classList.add('container2-right-right');

});

left3.addEventListener('click', ()=>{
    container3.classList.remove('container3-right');
    container2.classList.remove('container2-right-right');

});

